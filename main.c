#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>


typedef struct {
    int id;
    size_t size;
    char* text;
} s_message;


void listener();
void sender(s_message*, int);
void handler(int);


int main(int argc, char **argv) {
    size_t message_size;
    s_message *message;
    int destination_pid;

    if(argc != 2){
        printf("# Error. Usage: ./a.out <message>");
        return 1;
    }

    message_size = strlen(argv[1]);
    printf("# Message size is: %lu\n", message_size);
    printf("# Creating message object.\n");
    message = malloc(sizeof(s_message));
    message->id = 1;
    message->size = message_size;
    message->text = strdup(argv[1]);

    printf("# Forking...\n");
    if((destination_pid = fork()) == 0){
        listener();
        return 0;
    } else {
        sleep(1);
        sender(message, destination_pid);
    }

    printf("\033[0m# All done. Freeing memory.\n");
    free(message->text);
    free(message);
    return 0;
}

void listener(){
    printf("\033[0;35m# Listening on PID=%d\n", getpid());
    printf("\033[0;35m# Registering signal handler for SIGUSR1\n");
    signal(SIGUSR1, handler);
    // semicolon at the end - this is an endless loop.
    while(1);
}

void handler(int sig){
    signal(SIGUSR1, handler); // Reset signal
    printf("\033[0;33m# PID=%d received signal %d\n", getpid(), sig);

    key_t shm_key = ftok("./shared_path", 1);
    int shm_id = shmget(shm_key, 1024, 0666 | IPC_CREAT);
    s_message *sh_m = (s_message*) shmat(shm_id, (void*) 0, 0);
    // shmdt(sh_m);
    // shmctl(shm_id, IPC_RMID, NULL);

    printf("\033[0;33m# Received: %s\n", sh_m->text);
    exit(0);
}

void sender(s_message* m, int destination_pid){
    printf(
        "\033[0;36m# Writing message to shared memory a new message.\n\tid=%d\n\ttext:%s.\n",
        m->id,
        m->text
    );

    key_t shm_key = ftok("./shared_path", 1);
    int shm_id = shmget(shm_key, 1024, 0666 | IPC_CREAT);
    s_message *sh_m = (s_message*) shmat(shm_id, (void*) 0, 0);
    memcpy(sh_m, m, sizeof(s_message));
    shmdt(sh_m);

    kill(destination_pid, SIGUSR1);
}
